/**
 * Projet : ADN comptage ACTG
 * Auteur : Oscar Francois
 * Description : comptage du nombre d'acides nucleiques
 * Date : 2022-10-23-4
 * Version : 1.0
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adn_comptage_actg
{
    internal class Program
    {
        static void Main(string[] args)
        {
            StreamReader s;
            s = new StreamReader("chromosome-11-partial.txt");
            String line;
            line = s.ReadLine();

            int compterA = 0;
            int compterC = 0;
            int compterT = 0;
            int compterG = 0;

            while(line != null)
            {

                for (int i = 0; i < line.Length; i++)
                {
                    if (line[0] == 'A')
                    {
                        compterA++;
                    }
                    if (line[0] == 'C')
                    {
                        compterC++;
                    }
                    if (line[0] == 'T')
                    {
                        compterT++;
                    }
                    if (line[0] == 'G')
                    {
                        compterG++;
                    }
                }

                line = s.ReadLine();
            }

            Console.WriteLine("compter A = " + compterA);
            Console.WriteLine("compter C = " + compterC);
            Console.WriteLine("compter T = " + compterT);
            Console.WriteLine("compter G = " + compterG);
        }
    }
}
